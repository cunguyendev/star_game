import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// import App from './App';
import registerServiceWorker from './registerServiceWorker';

//  ReactDOM.render(<App />, document.getElementById('root'));
function tick() {
    const timer = (
        <b className="choose" >{new Date().toLocaleTimeString()}</b>
    );
    ReactDOM.render(timer, document.getElementById('timer'));
}
setInterval(tick, 1000);

// Set time 
let count = 5;
const message = (
    <b>You lose!</b>
);

function time_count() {
    const time_count = (
        <b className="choose" >({count})</b>
    );
    console.log('Time count is: '+count);
    count--;
    ReactDOM.render(time_count, document.getElementById('time_count'));
    if(count === 0){
        console.log('Time out...');
        // window.location.reload();
        // clearInterval(refreshIntervalId);
        alert('You lose!');
        ReactDOM.render(message, document.getElementById('time_count'));
    }
    
}
setInterval(time_count, 1000);

// Preloader
const preloader = (
    <div className="spinner">
        <span className="spinner-rotate"></span>
    </div>
);
ReactDOM.render(preloader, document.getElementById('preloader'));

registerServiceWorker();