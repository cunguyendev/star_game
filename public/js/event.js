// Event on click
function btn1_event() {
    $("choose").append("<button id='btn1-choosed' class='btn btn-default' onclick='btn1_remove();'>1</button>");
}

function btn2_event() {
    $("choose").append("<button id='btn2-choosed' class='btn btn-default' onclick='btn2_remove();'>2</button>");
}

function btn3_event() {
    $("choose").append("<button id='btn3-choosed' class='btn btn-default' onclick='btn3_remove();'>3</button>");
}

function btn4_event() {
    $("choose").append("<button id='btn4-choosed' class='btn btn-default' onclick='btn4_remove();'>4</button>");
}

function btn5_event() {
    $("choose").append("<button id='btn5-choosed' class='btn btn-default' onclick='btn5_remove();'>5</button>");
}

function btn6_event() {
    $("choose").append("<button id='btn6-choosed' class='btn btn-default' onclick='btn6_remove();'>6</button>");
}

function btn7_event() {
    $("choose").append("<button id='btn7-choosed' class='btn btn-default' onclick='btn7_remove();'>7</button>");
}

function btn8_event() {
    $("choose").append("<button id='btn8-choosed' class='btn btn-default' onclick='btn8_remove();'>8</button>");
}


function btn9_event() {
    $("choose").append("<button id='btn9-choosed' class='btn btn-default' onclick='btn9_remove();'>9</button>");
}

function btn10_event() {
    $("choose").append("<button id='btn10-choosed' class='btn btn-default' onclick='btn10_remove();'>10</button>");
}

// Event on remove
function btn1_remove() {
    $('#btnNumber1').removeAttr('disabled');
    $('#btn1-choosed').remove();
}

function btn2_remove() {
    $('#btnNumber2').removeAttr('disabled');
    $('#btn2-choosed').remove();
}

function btn3_remove() {
    $('#btnNumber3').removeAttr('disabled');
    $('#btn3-choosed').remove();
}

function btn4_remove() {
    $('#btnNumber4').removeAttr('disabled');
    $('#btn4-choosed').remove();
}

function btn5_remove() {
    $('#btnNumber5').removeAttr('disabled');
    $('#btn5-choosed').remove();
}

function btn6_remove() {
    $('#btnNumber6').removeAttr('disabled');
    $('#btn6-choosed').remove();
}

function btn7_remove() {
    $('#btnNumber7').removeAttr('disabled');
    $('#btn7-choosed').remove();
}

function btn8_remove() {
    $('#btnNumber8').removeAttr('disabled');
    $('#btn8-choosed').remove();
}

function btn9_remove() {
    $('#btnNumber9').removeAttr('disabled');
    $('#btn9-choosed').remove();
}

function btn10_remove() {
    $('#btnNumber10').removeAttr('disabled');
    $('#btn10-choosed').remove();
}

// Event random
function reset_random() {
    let numberOfRandom = Math.floor((Math.random() * 10) + 1);
    for (i = 0; i < numberOfRandom; i++) {
        $("star").append("<span class='fa fa-star checked'></span>");
    }
    document.getElementById("star").innerHTML = numberOfRandom;
    console.log("Reseting data...");
}

// Event random after load page
function random() { 
    const numberOfRandom = Math.floor((Math.random() * 10) + 1);
    for (i = 0; i < numberOfRandom; i++) {
        $("star").append("<span class='fa fa-star checked'></span>");
    }
    console.log("The random data after load page is: "+ numberOfRandom);
}

function cunguyen(){
    $('#star1').remove();
    console.log("Removed");
}
